gedit-plugins
=============

gedit-plugins is a set of additional plugins for the
[gedit](https://gedit-text-editor.org/) text editor.

See the
[gedit README.md](https://gitlab.gnome.org/World/gedit/gedit/-/blob/master/README.md)
file for more information (installation, how to contribute, etc).
