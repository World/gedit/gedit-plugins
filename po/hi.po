# translation of gedit.master.po to Hindi
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
#
# G Karunakar <karunakar@freedomink.org>, 2003.
# Ravishankar Shrivastava <raviratlami@yahoo.com>, 2004.
# Rajesh Ranjan <rranjan@redhat.com>, 2005, 2006, 2007, 2009.
# Rajesh Ranjan <rajesh672@gmail.com>, 2009.
# Scrambled777 <weblate.scrambled777@simplelogin.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: gedit.master\n"
"Report-Msgid-Bugs-To: https://gedit-technology.github.io/apps/gedit/"
"reporting-bugs.html\n"
"POT-Creation-Date: 2024-06-26 18:17+0000\n"
"PO-Revision-Date: 2024-06-27 10:42+0530\n"
"Last-Translator: Scrambled777 <weblate.scrambled777@simplelogin.com>\n"
"Language-Team: Hindi <indlinux-hindi@lists.sourceforge.net>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 46.1\n"

#: plugins/bookmarks/bookmarks.plugin.desktop.in.in:5
#: plugins/bookmarks/gedit-bookmarks.metainfo.xml.in:6
msgid "Bookmarks"
msgstr "पुस्तचिह्न"

#: plugins/bookmarks/bookmarks.plugin.desktop.in.in:6
#: plugins/bookmarks/gedit-bookmarks.metainfo.xml.in:7
msgid "Easy document navigation with bookmarks."
msgstr "पुस्तचिह्न के साथ आसान दस्तावेज नेविगेशन।"

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:141
msgid "Toggle Bookmark"
msgstr "पुस्तचिह्न टॉगल करें"

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:145
msgid "Go to Next Bookmark"
msgstr "अगले पुस्तचिह्न पर जाएं"

#: plugins/bookmarks/gedit-bookmarks-app-activatable.c:149
msgid "Go to Previous Bookmark"
msgstr "पिछली पुस्तचिह्न पर जाएं"

#: plugins/bracketcompletion/bracketcompletion.plugin.desktop.in.in:6
#: plugins/bracketcompletion/gedit-bracketcompletion.metainfo.xml.in:6
msgid "Bracket Completion"
msgstr "कोष्ठक समापन"

#: plugins/bracketcompletion/bracketcompletion.plugin.desktop.in.in:7
#: plugins/bracketcompletion/gedit-bracketcompletion.metainfo.xml.in:7
msgid "Automatically adds closing brackets."
msgstr "स्वचालित रूप से समापन कोष्ठक जोड़ता है।"

#: plugins/charmap/charmap/__init__.py:57
#: plugins/charmap/charmap.plugin.desktop.in.in:6
#: plugins/charmap/gedit-charmap.metainfo.xml.in:6
msgid "Character Map"
msgstr "वर्ण मानचित्र"

#: plugins/charmap/charmap.plugin.desktop.in.in:7
#: plugins/charmap/gedit-charmap.metainfo.xml.in:7
msgid "Insert special characters just by clicking on them."
msgstr "केवल उन पर क्लिक करके विशेष वर्ण सम्मिलित करें।"

#: plugins/codecomment/codecomment.plugin.desktop.in.in:6
#: plugins/codecomment/gedit-codecomment.metainfo.xml.in:6
msgid "Code Comment"
msgstr "कोड टिप्पणी"

#: plugins/codecomment/codecomment.plugin.desktop.in.in:7
#: plugins/codecomment/gedit-codecomment.metainfo.xml.in:7
msgid "Comment out or uncomment a selected block of code."
msgstr "कोड के किसी चयनित ब्लॉक पर टिप्पणी करें या टिप्पणी हटाएं।"

#: plugins/codecomment/codecomment.py:118
msgid "Co_mment Code"
msgstr "कोड टिप्पणी जोड़ें (_m)"

#: plugins/codecomment/codecomment.py:124
msgid "U_ncomment Code"
msgstr "कोड टिप्पणी हटाएं (_n)"

#: plugins/colorpicker/colorpicker.plugin.desktop.in.in:6
#: plugins/colorpicker/gedit-colorpicker.metainfo.xml.in:6
msgid "Color Picker"
msgstr "रंग चयनकर्ता"

#: plugins/colorpicker/colorpicker.plugin.desktop.in.in:7
#: plugins/colorpicker/gedit-colorpicker.metainfo.xml.in:7
msgid "Pick a color from a dialog and insert its hexadecimal representation."
msgstr "किसी संवाद से एक रंग चुनें और उसका हेक्साडेसिमल प्रतिनिधित्व डालें।"

#: plugins/colorpicker/colorpicker.py:132
msgid "Pick _Color…"
msgstr "रंग चुनें (_C)…"

#: plugins/colorpicker/colorpicker.py:172
msgid "Pick Color"
msgstr "रंग चुनें"

#: plugins/drawspaces/drawspaces.plugin.desktop.in.in:5
#: plugins/drawspaces/gedit-drawspaces.metainfo.xml.in:6
msgid "Draw Spaces"
msgstr "रिक्त स्थान बनाएं"

#: plugins/drawspaces/drawspaces.plugin.desktop.in.in:6
#: plugins/drawspaces/gedit-drawspaces.metainfo.xml.in:7
msgid "Draw spaces and tabs."
msgstr "रिक्त स्थान और टैब बनाएं।"

#: plugins/git/gedit-git.metainfo.xml.in:6
#: plugins/git/git.plugin.desktop.in.in:6
msgid "Git"
msgstr "Git"

#: plugins/git/gedit-git.metainfo.xml.in:7
#: plugins/git/git.plugin.desktop.in.in:7
msgid "Highlight lines that have been changed since the last commit."
msgstr "पिछले कमिट के बाद से बदली गई पंक्तियों को चिन्हांकित करें।"

#: plugins/joinlines/gedit-joinlines.metainfo.xml.in:6
#: plugins/joinlines/joinlines.plugin.desktop.in.in:6
msgid "Join/Split Lines"
msgstr "पंक्तियों को जोड़ें/विभाजित करें"

#: plugins/joinlines/gedit-joinlines.metainfo.xml.in:7
#: plugins/joinlines/joinlines.plugin.desktop.in.in:7
msgid "Join several lines or split long ones."
msgstr "कई पंक्तियों को जोड़ें या लंबी पंक्तियों को विभाजित करें।"

#: plugins/joinlines/joinlines.py:111
msgid "_Join Lines"
msgstr "पंक्तियां जोड़ें (_J)"

#: plugins/joinlines/joinlines.py:117
msgid "_Split Lines"
msgstr "पंक्तियां विभाजित करें (_S)"

#: plugins/multiedit/gedit-multiedit.metainfo.xml.in:6
#: plugins/multiedit/multiedit.plugin.desktop.in.in:6
msgid "Multi Edit"
msgstr "बहुसंपादन"

#: plugins/multiedit/gedit-multiedit.metainfo.xml.in:7
#: plugins/multiedit/multiedit.plugin.desktop.in.in:7
msgid "Edit document in multiple places at once."
msgstr "दस्तावेज को एक साथ कई स्थानों पर संपादित करें।"

#: plugins/multiedit/multiedit/appactivatable.py:44
#: plugins/multiedit/multiedit/viewactivatable.py:1351
msgid "Multi Edit Mode"
msgstr "बहु संपादन मोड"

#: plugins/multiedit/multiedit/viewactivatable.py:317
msgid "Added edit point…"
msgstr "संपादन बिंदु जोड़ा गया…"

#: plugins/multiedit/multiedit/viewactivatable.py:659
msgid "Column Mode…"
msgstr "स्तंभ मोड…"

#: plugins/multiedit/multiedit/viewactivatable.py:777
msgid "Removed edit point…"
msgstr "संपादन बिंदु हटाया गया…"

#: plugins/multiedit/multiedit/viewactivatable.py:943
msgid "Cancelled column mode…"
msgstr "रद्द किया गया स्तंभ मोड…"

#: plugins/multiedit/multiedit/viewactivatable.py:1306
msgid "Enter column edit mode using selection"
msgstr "चयन का उपयोग करके स्तंभ संपादन मोड दर्ज करें"

#: plugins/multiedit/multiedit/viewactivatable.py:1307
msgid "Enter <b>smart</b> column edit mode using selection"
msgstr "चयन का उपयोग करके <b>स्मार्ट</b> स्तंभ संपादन मोड दर्ज करें"

#: plugins/multiedit/multiedit/viewactivatable.py:1308
msgid "<b>Smart</b> column align mode using selection"
msgstr "<b>स्मार्ट</b> चयन का उपयोग करके स्तंभ संरेखण मोड"

#: plugins/multiedit/multiedit/viewactivatable.py:1309
msgid "<b>Smart</b> column align mode with additional space using selection"
msgstr "<b>स्मार्ट</b> स्तंभ संरेखण मोड चयन का उपयोग करके अतिरिक्त स्थान के साथ"

#: plugins/multiedit/multiedit/viewactivatable.py:1311
msgid "Toggle edit point"
msgstr "संपादन बिंदु टॉगल करें"

#: plugins/multiedit/multiedit/viewactivatable.py:1312
msgid "Add edit point at beginning of line/selection"
msgstr "पंक्ति/चयन की शुरुआत में संपादन बिंदु जोड़ें"

#: plugins/multiedit/multiedit/viewactivatable.py:1313
msgid "Add edit point at end of line/selection"
msgstr "पंक्ति/चयन के अंत में संपादन बिंदु जोड़ें"

#: plugins/multiedit/multiedit/viewactivatable.py:1314
msgid "Align edit points"
msgstr "संपादन बिंदु संरेखित करें"

#: plugins/multiedit/multiedit/viewactivatable.py:1315
msgid "Align edit points with additional space"
msgstr "अतिरिक्त स्थान के साथ संपादन बिंदु संरेखित करें"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:55
msgid "_Manage Saved Sessions…"
msgstr "सहेजे गये सत्र प्रबंधित करें (_M)…"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:58
msgid "_Save Session…"
msgstr "सत्र सहेजें (_S)…"

#: plugins/sessionsaver/sessionsaver/appactivatable.py:64
#, python-brace-format
msgid "Recover “{0}” Session"
msgstr "“{0}” सत्र पुनर्प्राप्त करें"

#: plugins/sessionsaver/sessionsaver/dialogs.py:153
msgid "Session Name"
msgstr "सत्र का नाम"

#: plugins/sessionsaver/sessionsaver.plugin.desktop.in.in:6
msgid "Session Saver"
msgstr "सत्र सहेजक"

#: plugins/sessionsaver/sessionsaver.plugin.desktop.in.in:7
msgid "Save and restore your working sessions."
msgstr "अपने कार्य सत्र सहेजें और पुनर्स्थापित करें।"

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:8
msgid "Save session"
msgstr "सत्र सहेजें"

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:76
msgid "Session name:"
msgstr "सत्र का नाम:"

#: plugins/sessionsaver/sessionsaver/ui/sessionsaver.ui:120
msgid "Saved Sessions"
msgstr "सहेजे गए सत्र"

#: plugins/smartspaces/gedit-smartspaces.metainfo.xml.in:6
#: plugins/smartspaces/smartspaces.plugin.desktop.in.in:5
msgid "Smart Spaces"
msgstr "स्मार्ट रिक्त स्थान"

#: plugins/smartspaces/gedit-smartspaces.metainfo.xml.in:7
#: plugins/smartspaces/smartspaces.plugin.desktop.in.in:6
msgid "Forget you’re not using tabulations."
msgstr "भूल जाइये कि आप सारणीकरणों का उपयोग नहीं कर रहे हैं।"

#: plugins/terminal/gedit-terminal.metainfo.xml.in:6
#: plugins/terminal/terminal.plugin.desktop.in.in:6
msgid "Embedded Terminal"
msgstr "अंतर्निहित टर्मिनल"

#: plugins/terminal/gedit-terminal.metainfo.xml.in:7
#: plugins/terminal/terminal.plugin.desktop.in.in:7
msgid "Embed a terminal in the bottom panel."
msgstr "निचले फलक में एक टर्मिनल अंतर्निहित करें।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:16
msgid "Whether to silence terminal bell"
msgstr "क्या टर्मिनल घंटी को शांत करना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:17
msgid ""
"If true, don’t make a noise when applications send the escape sequence for "
"the terminal bell."
msgstr ""
"यदि चयनित है, तो जब अनुप्रयोग टर्मिनल घंटी के लिए निकास अनुक्रम भेजते हैं तो शोर न करें।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:24
msgid "Number of lines to keep in scrollback"
msgstr "स्क्रॉलबैक में रखने के लिए पंक्तियों की संख्या"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:25
msgid ""
"Number of scrollback lines to keep around. You can scroll back in the "
"terminal by this number of lines; lines that don’t fit in the scrollback are "
"discarded. If scrollback-unlimited is true, this value is ignored."
msgstr ""
"आसपास रखने के लिए स्क्रॉलबैक लाइनों की संख्या। आप टर्मिनल में इस पंक्ति की संख्या से पीछे "
"स्क्रॉल कर सकते हैं; जो पंक्तियां स्क्रॉलबैक में फ़िट नहीं होतीं उन्हें हटा दिया जाता है। यदि "
"स्क्रॉलबैक-अनलिमिटेड चयनित है, तो इस मान को अनदेखा कर दिया जाता है।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:34
msgid "Whether an unlimited number of lines should be kept in scrollback"
msgstr "क्या स्क्रॉलबैक में असीमित संख्या में पंक्तियां रखी जानी चाहिए"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:37
msgid ""
"If true, scrollback lines will never be discarded. The scrollback history is "
"stored on disk temporarily, so this may cause the system to run out of disk "
"space if there is a lot of output to the terminal."
msgstr ""
"यदि चयनित है, तो स्क्रॉलबैक लाइनें कभी भी खारिज नहीं की जाएंगी। स्क्रॉलबैक इतिहास "
"अस्थायी रूप से डिस्क पर संग्रहीत किया जाता है, इसलिए यदि टर्मिनल पर बहुत अधिक आउटपुट है "
"तो इससे सिस्टम डिस्क स्थान से बाहर हो सकता है।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:46
msgid "Whether to scroll to the bottom when a key is pressed"
msgstr "क्या कुंजी दबाने पर नीचे तक स्क्रॉल करना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:47
msgid "If true, pressing a key jumps the scrollbar to the bottom."
msgstr "यदि चयनित है, तो कुंजी दबाने से स्क्रॉलबार नीचे की ओर चला जाता है।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:53
msgid "Whether to scroll to the bottom when there’s new output"
msgstr "क्या नया आउटपुट आने पर नीचे स्क्रॉल करना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:54
msgid ""
"If true, whenever there’s new output the terminal will scroll to the bottom."
msgstr ""
"यदि चयनित है, तो जब भी कोई नया आउटपुट आएगा तो टर्मिनल नीचे की ओर स्क्रॉल हो जाएगा।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:61
msgid "Default color of text in the terminal"
msgstr "टर्मिनल में पाठ का तयशुदा रंग"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:62
msgid ""
"Default color of text in the terminal, as a color specification (can be HTML-"
"style hex digits, or a color name such as “red”)."
msgstr ""
"टर्मिनल में पाठ का तयशुदा रंग, रंग विनिर्देश के रूप में (HTML-शैली हेक्स अंक, या “लाल” जैसा "
"रंग नाम हो सकता है)।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:70
msgid "Default color of terminal background"
msgstr "टर्मिनल पृष्ठभूमि का तयशुदा रंग"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:71
msgid ""
"Default color of terminal background, as a color specification (can be HTML-"
"style hex digits, or a color name such as “red”)."
msgstr ""
"रंग विनिर्देश के रूप में टर्मिनल पृष्ठभूमि का तयशुदा रंग (HTML-शैली हेक्स अंक, या “लाल” जैसा "
"रंग नाम हो सकता है)।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:94
msgid "Palette for terminal applications"
msgstr "टर्मिनल अनुप्रयोगों के लिए पैलेट"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:95
msgid ""
"Terminals have a 16-color palette that applications inside the terminal can "
"use. This is that palette, in the form of a colon-separated list of color "
"names. Color names should be in hex format e.g. “#FF00FF”"
msgstr ""
"टर्मिनलों में 16-रंग का पैलेट होता है जिसका उपयोग टर्मिनल के अंदर के अनुप्रयोग कर सकते हैं। "
"यह वह पैलेट है, जो रंग नामों की कोलन-पृथक सूची के रूप में है। रंग के नाम हेक्स प्रारूप में होने "
"चाहिए जैसे “#FF00FF”"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:104
msgid "Whether to use the colors from the theme for the terminal widget"
msgstr "क्या टर्मिनल विजेट के लिए थीम से रंगों का उपयोग करना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:107
msgid ""
"If true, the theme color scheme used for text entry boxes will be used for "
"the terminal, instead of colors provided by the user."
msgstr ""
"यदि चयनित है, तो उपयोगकर्ता द्वारा प्रदान किए गए रंगों के बजाय, पाठ प्रविष्टि बक्से के "
"लिए उपयोग की जाने वाली थीम रंग योजना का उपयोग टर्मिनल के लिए किया जाएगा।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:114
msgid "Whether to blink the cursor"
msgstr "क्या कर्सर को झपकाना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:115
msgid ""
"The possible values are “system” to use the global cursor blinking settings, "
"or “on” or “off” to set the mode explicitly."
msgstr ""
"वैश्विक कर्सर झपकने की सेटिंग्स का उपयोग करने के लिए संभावित मान “सिस्टम” हैं, या मोड को "
"स्पष्ट रूप से सेट करने के लिए “चालू” या “बंद” हैं।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:122
msgid "The cursor appearance"
msgstr "कर्सर की दिखावट"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:123
msgid ""
"The possible values are “block” to use a block cursor, “ibeam” to use a "
"vertical line cursor, or “underline” to use an underline cursor."
msgstr ""
"ब्लॉक कर्सर का उपयोग करने के लिए संभावित मान “ब्लॉक”, ऊर्ध्वाधर लाइन कर्सर का उपयोग "
"करने के लिए “आईबीम”, या अंडरलाइन कर्सर का उपयोग करने के लिए “रेखांकन” हैं।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:130
msgid "Whether to use the system font"
msgstr "क्या सिस्टम फॉन्ट का उपयोग करना है"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:131
msgid ""
"If true, the terminal will use the desktop-global standard font if it’s "
"monospace (and the most similar font it can come up with otherwise)."
msgstr ""
"यदि चयनित है, तो टर्मिनल डेस्कटॉप-वैश्विक मानक फॉन्ट का उपयोग करेगा यदि यह मोनोस्पेस है "
"(और अन्यथा यह सबसे समान फॉन्ट के साथ आ सकता है)।"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:139
msgid "Font"
msgstr "फॉन्ट"

#: plugins/terminal/org.gnome.gedit.plugins.terminal.gschema.xml:140
msgid "A Pango font name. Examples are “Sans 12” or “Monospace Bold 14”."
msgstr "एक पैंगो फॉन्ट नाम। उदाहरण “सैन्स 12” या “मोनोस्पेस बोल्ड 14” हैं।"

#: plugins/terminal/terminal.py:342
msgid "Terminal"
msgstr "टर्मिनल"

#: plugins/terminal/terminal.py:365
msgid "C_hange Directory"
msgstr "निर्देशिका बदलें (_h)"

#: plugins/textsize/gedit-textsize.metainfo.xml.in:6
#: plugins/textsize/textsize.plugin.desktop.in.in:6
msgid "Text Size"
msgstr "पाठ का आकार"

#: plugins/textsize/gedit-textsize.metainfo.xml.in:7
#: plugins/textsize/textsize.plugin.desktop.in.in:7
msgid "Easily increase and decrease the text size."
msgstr "पाठ का आकार आसानी से बढ़ाएं और घटाएं।"

#: plugins/textsize/textsize/__init__.py:53
msgid "_Normal size"
msgstr "सामान्य आकार (_N)"

#: plugins/textsize/textsize/__init__.py:55
msgid "S_maller Text"
msgstr "छोटा पाठ (_m)"

#: plugins/textsize/textsize/__init__.py:57
msgid "_Larger Text"
msgstr "बड़ा पाठ (_L)"

#: plugins/wordcompletion/gedit-word-completion-configure.ui:18
msgid "Interactive completion"
msgstr "अंतर्क्रियात्मक समापन"

#: plugins/wordcompletion/gedit-word-completion-configure.ui:43
msgid "Minimum word size:"
msgstr "न्यूनतम शब्द आकार:"

#: plugins/wordcompletion/gedit-wordcompletion.metainfo.xml.in:6
#: plugins/wordcompletion/wordcompletion.plugin.desktop.in.in:5
msgid "Word Completion"
msgstr "शब्द समापन"

#: plugins/wordcompletion/gedit-wordcompletion.metainfo.xml.in:7
#: plugins/wordcompletion/wordcompletion.plugin.desktop.in.in:6
msgid "Auto-completion using words already present in open documents."
msgstr "दस्तावेज में पहले से मौजूद शब्दों का उपयोग करके स्वचालित समापन का प्रस्ताव रखें।"

#: plugins/wordcompletion/gedit-word-completion-plugin.c:181
msgid "Document Words"
msgstr "दस्तावेज शब्द"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:5
msgid "Interactive Completion"
msgstr "अंतर्क्रियात्मक समापन"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:6
msgid "Whether to enable interactive completion."
msgstr "क्या अंतर्क्रियात्मक समापन सक्षम करना है।"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:11
msgid "Minimum Word Size"
msgstr "न्यूनतम शब्द आकार"

#: plugins/wordcompletion/org.gnome.gedit.plugins.wordcompletion.gschema.xml:12
msgid "The minimum word size to complete."
msgstr "पूर्ण करने के लिए न्यूनतम शब्द आकार।"
